POP Calendar
============

## Robust calendar synchronization between POP and Android

POP Calendar is an Android application for syncing POP (Tampere university of technology
portal for students) calendar with the stock Android calendar. The app only supports one-way
synchronisation of lectures, excercises, examns and user events.

The app uses TUT intranet username and password to log in to POP portal, downloads the calendar
XML -file, parses it and syncs it with the stock calendar's database.

I created this application when I bought my first Android phone and wanted to get familiar with Java
and Android SDK. This was my first real Java (and Android) project, and I feel it taught me a great deal
about mobile app developement and Java programming language.

Author
------
Matti Määttä ([maatta.matti@gmail.com])

Screenshots
-----------

[![screenshot](https://bitbucket.org/maattam/popcalendar/raw/5617aa53bed775d74dc20646e039b0286d1d185d/images/main_small.png)](https://bitbucket.org/maattam/popcalendar/raw/80ea6742f18b885c011c22fee53a28d57c1205b3/images/main.png)

[maatta.matti@gmail.com]: mailto:maatta.matti@gmail.com