package com.lese.popcalendar.calendar;

import java.util.ArrayList;
import java.util.List;

import com.lese.popcalendar.calendar.api.CalendarApi;
import com.lese.popcalendar.calendar.api.CalendarApi14;
import com.lese.popcalendar.calendar.api.CalendarApi8;

import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;

public class CalendarProvider {
	private final static String mAccountName = "com.lese.popcalendar";
	private final ContentResolver mResolver;
	private long mCalendarId = -1;
	
	public static final CalendarApi api = getApi();
	private static CalendarApi getApi() {
		if(android.os.Build.VERSION.SDK_INT >= 14) {
			return new CalendarApi14();

        // Since android calendar api came with sdk version 14, we revert to an unofficial
        // hack to expose android.calendar tables on older api versions (only works with gapps calendar)
		} else if(android.os.Build.VERSION.SDK_INT >= 8) {
			return new CalendarApi8();
		}
		
		return null; // Not supported
	}
	
	public CalendarProvider(ContentResolver resolver)
			throws Exception {
		mResolver = resolver;
		
		if(api == null) {
			throw new Exception("Sorry, your Android version is not supported.");
		}
	}
	
	private Cursor fetchEvents(long begin, long end) {
		final String[] projection = new String[] {
				api.getEvent(CalendarApi.EVENT_ID),
				api.getEvent(CalendarApi.EVENT_DTSTART),
				api.getEvent(CalendarApi.EVENT_DTEND),
				api.getEvent(CalendarApi.EVENT_TITLE),
				api.getEvent(CalendarApi.EVENT_DESCRIPTION),
				api.getEvent(CalendarApi.EVENT_LOCATION) };
		
		final String selection = String.format("%s=? and %s>=? and %s<=?",
				api.getEvent(CalendarApi.EVENT_CALENDAR_ID),
				api.getEvent(CalendarApi.EVENT_DTSTART),
				api.getEvent(CalendarApi.EVENT_DTSTART));
		
		String[] args = new String[] { Long.toString(getCalendarId()),
				Long.toString(begin), Long.toString(end) };
		
		return mResolver.query(api.getEventsUri(), projection,
				selection, args,
				api.getEvent(CalendarApi.EVENT_DTSTART) + " asc");
	}
	
	private ContentProviderOperation buildDeleteEvent(long eventId) {
		return ContentProviderOperation.newDelete(api.getEventsUri())
				.withSelection(api.getEvent(CalendarApi.EVENT_ID) + "=?",
						new String[]{ Long.toString(eventId) })
				.withYieldAllowed(true)
				.build();
	}
	
	private ContentProviderOperation buildInsertEvent(ContentValues values) {
		return ContentProviderOperation.newInsert(api.getEventsUri())
				.withValues(values)
				.build();
	}
	
	public int clear() throws Exception {
		// Fetch all events
		Cursor cursor = mResolver.query(api.getEventsUri(),
				new String[] { api.getEvent(CalendarApi.EVENT_ID) },
				api.getEvent(CalendarApi.EVENT_CALENDAR_ID) + "=?",
				new String[] {Long.toString(getCalendarId())},
				null);

		int deleted = 0;
		if(cursor.moveToFirst()) {
			ArrayList<ContentProviderOperation> ops =
					new ArrayList<ContentProviderOperation>();
			
			do {
				ops.add(buildDeleteEvent(cursor.getLong(0)));
				++deleted;
			} while(cursor.moveToNext());
			
			if(ops.size() > 0) {
				mResolver.applyBatch(
						api.getContract(CalendarApi.CONTRACT_AUTHORITY), ops);
			}
		}
		
		cursor.close();
		return deleted;
	}
	
	public int[] synchronize(long begin, long end, List<ContentValues> events)
					throws RemoteException, OperationApplicationException {
		int newEvents = 0, updatedEvents = 0, deletedEvents = 0;
		
		// Fetch all event instances between begin and end
		Cursor cursor = fetchEvents(begin, end);
		
		boolean next = cursor.moveToFirst();
		ArrayList<ContentProviderOperation> ops =
				new ArrayList<ContentProviderOperation>();
		int index = 0;
		
		while(next) {
			if(index < events.size()) {
				ContentValues event = events.get(index);
				long start = event.getAsLong(api.getEvent(CalendarApi.EVENT_DTSTART));
				
				// Does event occur before current cursor?
				if(start < cursor.getLong(1)) {
					ops.add(buildInsertEvent(event));

					++newEvents;
					++index;
					continue;
				}
				
				// Do events occur at the same time?
				else if(start == cursor.getLong(1)) {
					ContentValues diffs = getEventDiffs(cursor, event);
					
					// If title mismatch, delete old event entirely
					if(diffs.containsKey(api.getEvent(CalendarApi.EVENT_TITLE))) {
						ops.add(buildDeleteEvent(cursor.getLong(0)));
						ops.add(buildInsertEvent(event));

						++deletedEvents; ++newEvents;
					}
					
					// Update event if one or more fields differ
					else if(diffs.size() > 0) {
						ops.add(ContentProviderOperation.newUpdate(api.getEventsUri())
								.withSelection(api.getEvent(CalendarApi.EVENT_ID) + "=?",
										new String[] { Long.toString(cursor.getLong(0)) })
								.withValues(diffs)
								.build());

						++updatedEvents;
					}
					
					// No update required
					++index;
				}
				
				else {
					ops.add(buildDeleteEvent(cursor.getLong(0)));

					++deletedEvents;
				}
			}
			
			else {
				ops.add(buildDeleteEvent(cursor.getLong(0)));

				++deletedEvents;
			}
			
			next = cursor.moveToNext();
		}
		
		// Push rest of the events
		while(index < events.size()) {
			ops.add(buildInsertEvent(events.get(index)));

			++newEvents;
			++index;
		}
		
		// Execute mods
		if(ops.size() > 0) {
			mResolver.applyBatch(
					api.getContract(CalendarApi.CONTRACT_AUTHORITY), ops);
		}
		
		cursor.close();
		return new int[] {newEvents, updatedEvents, deletedEvents };
	}
	
	private ContentValues getEventDiffs(Cursor old,
			ContentValues newval) {
		ContentValues diffs = new ContentValues();
		
		String key = null;
		for(int i = 1; i < 6; ++i) {	// skip eventid
			key = api.getEvent(i);
			
			if(!old.getString(i).equals(newval.getAsString(key))) {
				diffs.put(key, newval.getAsString(key));
			}
		}
		
		return diffs;
	}
	
	/*private void setReminder(long eventId, int minutes) {
		ContentValues values = new ContentValues();
		values.put(Reminders.EVENT_ID, eventId);
		values.put(Reminders.METHOD, Reminders.METHOD_ALERT);
		values.put(Reminders.MINUTES, minutes);
		
		mResolver.insert(Reminders.CONTENT_URI, values);
	}*/
	
	public long getCalendarId() {
		if(mCalendarId == -1) {
			String[] projection = new String[] {
					api.getCalendar(CalendarApi.CALENDAR_ID) };
			
			String selection =
					api.getCalendar(CalendarApi.CALENDAR_ACCOUNT_NAME) + "=? AND " +
					api.getCalendar(CalendarApi.CALENDAR_ACCOUNT_TYPE) + "=?";
			
			String[] args = new String[] {
					mAccountName,
					api.getContract(CalendarApi.CONTRACT_ACCOUNT_TYPE_LOCAL) };

			Cursor cursor = mResolver.query(api.getCalendarsUri(),
					projection, selection, args, null);
			
			if(cursor.moveToFirst()) {
				mCalendarId = cursor.getLong(0);
				cursor.close();
			} else {
				return -1; // Calendar not created
			}
		}
		
		return mCalendarId;
	}
	
	public long createCalendar() throws Exception {
		ContentValues values = new ContentValues();
		values.put(api.getCalendar(CalendarApi.CALENDAR_ACCOUNT_NAME),
				mAccountName);
		values.put(api.getCalendar(CalendarApi.CALENDAR_ACCOUNT_TYPE),
				api.getContract(CalendarApi.CONTRACT_ACCOUNT_TYPE_LOCAL));
		values.put(api.getCalendar(CalendarApi.CALENDAR_NAME),
				"TUT POP Calendar");
		values.put(api.getCalendar(CalendarApi.CALENDAR_CALENDAR_DISPLAY_NAME),
				"TUT POP Calendar");
		values.put(api.getCalendar(CalendarApi.CALENDAR_CALENDAR_COLOR),
				0xffff9900);
		values.put(api.getCalendar(CalendarApi.CALENDAR_CALENDAR_ACCESS_LEVEL),
				api.getCalendarAccessOwner());
		values.put(api.getCalendar(CalendarApi.CALENDAR_OWNER_ACCOUNT),
				mAccountName);
		values.put(api.getCalendar(CalendarApi.CALENDAR_CALENDAR_TIME_ZONE),
				"Europe/Helsinki");
		// Attempt to fix pre-ics calendars
		values.put(api.getCalendar(CalendarApi.CALENDAR_SYNC_EVENTS), "1");
		values.put(api.getCalendar(CalendarApi.CALENDAR_VISIBLE), "1");
		
		Uri.Builder builder = api.getCalendarsUri().buildUpon();
		builder.appendQueryParameter(
				api.getCalendar(CalendarApi.CALENDAR_ACCOUNT_NAME), mAccountName);
		builder.appendQueryParameter(
				api.getCalendar(CalendarApi.CALENDAR_ACCOUNT_TYPE),
				api.getContract(CalendarApi.CONTRACT_ACCOUNT_TYPE_LOCAL));
		builder.appendQueryParameter(
				api.getContract(CalendarApi.CONTRACT_CALLER_IS_SYNCADAPTER), "true");
		
		Uri uri = mResolver.insert(builder.build(), values);
		mCalendarId = Long.valueOf(uri.getLastPathSegment());
		
		if(mCalendarId == -1) {
			throw new Exception("Insert returned invalid index");
		}
		
		return mCalendarId;
	}
	
	public boolean deleteCalendar() {
		if(mCalendarId == -1) {
			return false;
		}
		
		return mResolver.delete(api.getCalendarsUri(),
				api.getCalendar(CalendarApi.CALENDAR_ID) + "=?",
				new String[] { Long.toString(mCalendarId) }) > 0;
	}
}
