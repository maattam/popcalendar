package com.lese.popcalendar.calendar.api;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;

@SuppressLint("NewApi")
public class CalendarApi14 implements CalendarApi {
	private static final String[] EVENT_TABLE = new String[] {
		Events._ID,
		Events.DTSTART,
		Events.DTEND,
		Events.TITLE,
		Events.DESCRIPTION,
		Events.EVENT_LOCATION,
		Events.EVENT_TIMEZONE,
		Events.CALENDAR_ID			// 0x7
	};
	
	private static final String[] CALENDAR_TABLE = new String[] {
		Calendars._ID,
		Calendars.ACCOUNT_NAME,
		Calendars.ACCOUNT_TYPE,
		Calendars.NAME,
		Calendars.CALENDAR_DISPLAY_NAME,
		Calendars.CALENDAR_COLOR,
		Calendars.CALENDAR_ACCESS_LEVEL,
		Calendars.OWNER_ACCOUNT,
		Calendars.CALENDAR_TIME_ZONE,		// 0x8
		Calendars.SYNC_EVENTS,
		Calendars.VISIBLE
	};
	
	private static final String[] CONTRACT_TABLE = new String[] {
		CalendarContract.ACCOUNT_TYPE_LOCAL,
		CalendarContract.CALLER_IS_SYNCADAPTER,
		CalendarContract.AUTHORITY		// 0x2
	};
	
	public int getCalendarAccessOwner() {
		return Calendars.CAL_ACCESS_OWNER;
	}

	public String getEvent(int id) {
		return EVENT_TABLE[id];
	}

	public String getCalendar(int id) {
		return CALENDAR_TABLE[id];
	}

	public String getContract(int id) {
		return CONTRACT_TABLE[id];
	}

	public Uri getCalendarsUri() {
		return Calendars.CONTENT_URI;
	}

	public Uri getEventsUri() {
		return Events.CONTENT_URI;
	}
}
