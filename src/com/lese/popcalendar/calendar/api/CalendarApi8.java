package com.lese.popcalendar.calendar.api;

import android.net.Uri;

public class CalendarApi8 implements CalendarApi {
	private static final String[] EVENT_TABLE = new String[] {
		"_id",
		"dtstart",
		"dtend",
		"title",
		"description",
		"eventLocation",
		"eventTimezone",
		"calendar_id"			// 0x7
	};
	
	private static final String[] CALENDAR_TABLE = new String[] {
		"_id",
		"_sync_account",
		"_sync_account_type",
		"name",
		"displayName",
		"color",
		"access_level",
		"ownerAccount",
		"timezone",			// 0x8
		"sync_events",
		"selected"
	};
	
	private static final String[] CONTRACT_TABLE = new String[] {
		"LOCAL",
		"caller_is_syncadapter",
		"com.android.calendar"		// 0x2
	};
	
	public int getCalendarAccessOwner() { return 700; }

	public String getEvent(int id) {
		return EVENT_TABLE[id];
	}

	public String getCalendar(int id) {
		return CALENDAR_TABLE[id];
	}

	public String getContract(int id) {
		return CONTRACT_TABLE[id];
	}

	public Uri getCalendarsUri() {
		return Uri.parse("content://com.android.calendar/calendars");
	}

	public Uri getEventsUri() {
		return Uri.parse("content://com.android.calendar/events");
	}
}
