package com.lese.popcalendar.calendar.api;

import android.net.Uri;

public interface CalendarApi {
	public static final int EVENT_ID = 0;
	public static final int EVENT_DTSTART = 1;
	public static final int EVENT_DTEND = 2;
	public static final int EVENT_TITLE = 3;
	public static final int EVENT_DESCRIPTION = 4;
	public static final int EVENT_LOCATION = 5;
	public static final int EVENT_EVENT_TIMEZONE = 6;
	public static final int EVENT_CALENDAR_ID = 7;
	
	public static final int CALENDAR_ID = 0;
	public static final int CALENDAR_ACCOUNT_NAME = 1;
	public static final int CALENDAR_ACCOUNT_TYPE = 2;
	public static final int CALENDAR_NAME = 3;
	public static final int CALENDAR_CALENDAR_DISPLAY_NAME = 4;
	public static final int CALENDAR_CALENDAR_COLOR = 5;
	public static final int CALENDAR_CALENDAR_ACCESS_LEVEL = 6;
	public static final int CALENDAR_OWNER_ACCOUNT = 7;
	public static final int CALENDAR_CALENDAR_TIME_ZONE = 8;
	public static final int CALENDAR_SYNC_EVENTS = 9;
	public static final int CALENDAR_VISIBLE = 10;
	
	public static final int CONTRACT_ACCOUNT_TYPE_LOCAL = 0;
	public static final int CONTRACT_CALLER_IS_SYNCADAPTER = 1;
	public static final int CONTRACT_AUTHORITY = 2;
	
	public static final int EVENT_SIZE = EVENT_CALENDAR_ID + 1;
	public static final int CALENDAR_SIZE = CALENDAR_VISIBLE + 1;
	public static final int CONTRACT_SIZE = CONTRACT_AUTHORITY + 1;
	
	public int getCalendarAccessOwner();
	
	public String getEvent(int id);
	public String getCalendar(int id);
	public String getContract(int id);
	
	public Uri getCalendarsUri();
	public Uri getEventsUri();
}
