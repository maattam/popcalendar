package com.lese.popcalendar.calendar;

import android.annotation.SuppressLint;
import android.text.Html;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.HttpResponse;

public class POPHandler {
	private static final String POST_LOGIN = "https://idp.tut.fi/idp/Authn/UserPassword";
	private static final String REQUEST_URL = "https://pop-portal.tut.fi/portal/page/portal/POP-portaali/10Etusivu/Tyosivu/Kalenteri";
	private static final String POST_REDIRECT = "https://pop-sso.tut.fi/Shibboleth.sso/SAML2/POST";
	
	private static POPHandler mInstance = null;
	public static POPHandler getInstance() {
		if(mInstance == null) {
			mInstance = new POPHandler();
		}
		
		return mInstance;
	}
	
	public static void closeInstance() {
		if(mInstance != null) {
			mInstance.resetSession();
			mInstance = null;
		}
	}
	
	private POPHandler() {}
	
	private String mTargetPost = null;
	private CookieStore mCookies = null;
	private DefaultHttpClient mClient = null;
	
	private HttpClient getHttpClient() {
		if(mClient == null) {
			List<Header> headers = new ArrayList<Header>();
			headers.add(new BasicHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));
			headers.add(new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64)"));
			headers.add(new BasicHeader("Accept-Language", "fi-FI,fi;q=0.8,en-US;q=0.6,en;q=0.4"));
			headers.add(new BasicHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3"));
			
			HttpParams params = new BasicHttpParams();
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			params.setParameter(ClientPNames.DEFAULT_HEADERS, headers);
			HttpConnectionParams.setConnectionTimeout(params, 5000);
			HttpConnectionParams.setSoTimeout(params, 10000);
		
			mClient = new DefaultHttpClient(params);
			if(mCookies == null) {
				mCookies = mClient.getCookieStore();
			} else {
				mClient.setCookieStore(mCookies);
			}
		}

		return mClient;
	}
	
	// Safe to call from ui thread
	public void resetSession() {
		mTargetPost = null;
		
		if(mCookies != null) {
			mCookies.clear();
			mCookies = null;
		}
	}
	
	// Never call from ui thread
	public void closeConnection() {
		if(mClient != null) {
			mClient.getConnectionManager().shutdown();
			mClient = null;
		}
	}
	
	public boolean access(String username, String password) throws IOException, Exception {
		// Are we already logged in?
		if(isAuthorizedSession()) {
			return true;
		}

		resetSession();
		HttpClient httpClient = getHttpClient();
		HttpResponse response = null;
		
		try {
			// Retrieve calendar page and set redirect cookie
			response = httpClient.execute(new HttpGet(REQUEST_URL));
			if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				throw new IOException("Unable to reach login page:\n"
						+ response.getStatusLine().getReasonPhrase());
			}
			response.getEntity().consumeContent();

			// Send login form
			response = postLogin(username, password, httpClient);
		
			// Parse redirect response
			List<NameValuePair> nvps = parseLoginResponse(response.getEntity());
			
			if(nvps.size() != 2) {
				return false; // Invalid login
			}
			
			// Send redirect response
			response = postRedirect(nvps, httpClient);
			mTargetPost = readCalendarForm(response.getEntity());

			if(mTargetPost == null) {
				throw new IOException("Invalid response after login");
			}
			
		} finally {
			if(response != null && response.getEntity().isStreaming()) {
				response.getEntity().consumeContent();
			}
		}

		return true;
	}
	
	private String readCalendarForm(HttpEntity entity)
			throws Exception {
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(entity.getContent(), "UTF-8"));
		
		String result = null;
		String line = null;
		while((line = reader.readLine()) != null) {
            // Calendar export action url
			if(line.contains("<form id=\"xmlOutputForm\"")) {
				result = readValue("action=\"", "\">", line);
				break;
			}
		}
		
		entity.consumeContent();
		return result;
	}
	
	private HttpResponse postRedirect(List<NameValuePair> nvps, HttpClient client)
			throws IOException {
		HttpPost post = new HttpPost(POST_REDIRECT);
		post.addHeader("Content-Type", "application/x-www-form-urlencoded");
		post.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

		HttpResponse response = client.execute(post);
		if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			throw new IOException("POP-Portal redirect failed:\n"
					+ response.getStatusLine().getReasonPhrase());
		}
		
		return response;
	}
	
	private List<NameValuePair> parseLoginResponse(HttpEntity entity)
			throws Exception {
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(entity.getContent(), "UTF-8"));

		String line = null;
		while((line = reader.readLine()) != null) {
            // POP needs RelayState and SAMLResponse fields to handle our login
			if(line.contains("RelayState")) {
				nvps.add(new BasicNameValuePair("RelayState",
						readValue("value=\"", "\"/>", line)));
				
			} else if(line.contains("SAMLResponse")) {
				nvps.add(new BasicNameValuePair("SAMLResponse",
						readValue("value=\"", "\"/>", line)));
			}
			
			if(nvps.size() == 2) {
				break;
			}
		}
		
		entity.consumeContent();
		
		return nvps;
	}
	
	private String readValue(String pre, String end, String line) {
		int first = line.indexOf(pre);
		int last = line.indexOf(end);
		
		if(first == -1 || last == -1) {
			return null;
		}
		
		String str = line.substring(first + pre.length(), last);
		return Html.fromHtml(str).toString();
	}
	
	private HttpResponse postLogin(String user, String password, HttpClient client)
			throws IOException {
		HttpPost post = new HttpPost(POST_LOGIN);
		post.addHeader("Content-Type", "application/x-www-form-urlencoded");
		
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		
		nvps.add(new BasicNameValuePair("service_provider", "pop-sso.tut.fi"));
		nvps.add(new BasicNameValuePair("j_username", user));
		nvps.add(new BasicNameValuePair("j_password", password));
		nvps.add(new BasicNameValuePair("confirm", "1"));
		
		post.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

		HttpResponse response = client.execute(post);
		if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			throw new IOException("Unable to reach POP-Portal redirect:\n"
					+ response.getStatusLine().getReasonPhrase());
		}
		
		return response;
	}
	
	private boolean isAuthorizedSession() {
		if(mTargetPost == null || mCookies == null) {
			return false;
		}

        // TODO: Handle cookie expiration (?)
		for(Cookie cookie : mCookies.getCookies()) {
			if(cookie.getName().equals("POP_SESSION_COOKIE")) {
				return true;
			}
		}
		
		return false;
	}
	
	@SuppressLint("SimpleDateFormat")
	public InputStream downloadCalendar(long start, long end) throws IOException, Exception {
		if(!isAuthorizedSession()) {
			throw new Exception("Invalid session.");
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		StringEntity se = new StringEntity("xmlOutputFormSubmit=xmlOutputFormSubmit&xmlStartDate="
                + sdf.format(new Date(start)) + "&xmlEndDate="
                + sdf.format(new Date(end)));
		
		HttpClient client = getHttpClient();
		HttpPost post = new HttpPost(mTargetPost);
		post.addHeader("Content-Type", "application/x-www-form-urlencoded");
		post.setEntity(se);
			
		HttpResponse response = client.execute(post);
		if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			throw new IOException("Calendar POST failed:\n"
					+ response.getStatusLine().getReasonPhrase());
		}
			
		// Read file url and download it
		String file = readCalendarPost(response.getEntity());
		if(file == null) {
			throw new IOException("Invalid calendar POST response");
		}
			
		response = client.execute(new HttpGet(file));
		if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			throw new IOException("Unable to download calendar:\n"
					+ response.getStatusLine().getReasonPhrase());
		}
			
		// Read xml response
		return response.getEntity().getContent();
	}
	
	private String readCalendarPost(HttpEntity entity) throws Exception {
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(entity.getContent(), "UTF-8"));

		String result = null;
		String line = null;
		while((line = reader.readLine()) != null) {
            // POP uses javascript to open a new window with the calendar URL
			if(line.contains("openWindowLarge(\"")) {
				result = readValue("openWindowLarge(\"", "\",", line);
				break;
			}
		}
		
		entity.consumeContent();
		return result;
	}
}
