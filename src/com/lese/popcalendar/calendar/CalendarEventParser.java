package com.lese.popcalendar.calendar;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.lese.popcalendar.calendar.api.CalendarApi;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.util.Xml;

public class CalendarEventParser {
	private final long mCalendarId;
	private static final String ns = null; // Skip namespaces
	
	public CalendarEventParser(long calId) {
		mCalendarId = calId;
	}
	
	public List<ContentValues> parse(InputStream input)
			throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(input, null);
			parser.nextTag();
			
			return readCalendarNotes(parser);
		} finally {
			input.close();
		}
	}
	
	private List<ContentValues> readCalendarNotes(XmlPullParser parser)
			throws XmlPullParserException, IOException {

		List<ContentValues> entries = new ArrayList<ContentValues>();
		
		parser.require(XmlPullParser.START_TAG, ns, "CalendarNotes");
		while(parser.next() != XmlPullParser.END_TAG) {
			if(parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			
			if(parser.getName().equals("CalendarNote")) {
				entries.add(readCalendarNote(parser));
			} else {
				skip(parser);
			}
		}
		
		Collections.sort(entries, new Comparator<ContentValues>() {
			@Override
			public int compare(ContentValues a, ContentValues b) {
				CalendarApi api = CalendarProvider.api;
				Long ta = a.getAsLong(api.getEvent(CalendarApi.EVENT_DTSTART));
				Long tb = b.getAsLong(api.getEvent(CalendarApi.EVENT_DTSTART));
				return ta.compareTo(tb);
			}
		});
		
		return entries;
	}
	
	@SuppressLint("SimpleDateFormat")
	private ContentValues readCalendarNote(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "CalendarNote");
		
		ContentValues entry = new ContentValues();
		CalendarApi api = CalendarProvider.api;
		// Merge these
		//String layer = "";
		String description = "";
		String link = "";
		String dateBegin = null, timeBegin = null;
		String dateEnd = null, timeEnd = null;
		
		while(parser.next() != XmlPullParser.END_TAG) {
			if(parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			
			// Unhandled: IsRecurrentEvent, IsAlarm
			String name = parser.getName();
			if(name.equals("Title")) {
				entry.put(api.getEvent(CalendarApi.EVENT_TITLE),
						readValue("Title", parser));
				
			} /*else if(name.equals("Layer")) {
				layer = readValue("Layer", parser);
				
			}*/ else if(name.equals("StartDate")) {
				dateBegin = readValue("StartDate", parser);
				
			} else if(name.equals("BeginTime")) {
				timeBegin = readValue("BeginTime", parser);
				
			} else if(name.equals("EndDate")) {
				dateEnd = readValue("EndDate", parser);
				
			} else if(name.equals("EndTime")) {
				timeEnd = readValue("EndTime", parser);
				
			} else if(name.equals("Description")) {
				description = readValue("Description", parser);
				
			} else if(name.equals("Link")) {
				link = readValue("Link", parser);
				
			} else if(name.equals("Location")) {
				entry.put(api.getEvent(CalendarApi.EVENT_LOCATION),
						readValue("Location", parser));
			} else {
				skip(parser);
			}
		}
		
		// Format android description
		String newDesc = new String();
		/*if(layer.length() > 0) {
			newDesc += layer;
		}*/
		
		if(description.length() > 0) {
			newDesc += description;
		}
		
		if(link.length() > 0) {
			newDesc += " url: " + link;
		}
		
		entry.put(api.getEvent(CalendarApi.EVENT_DESCRIPTION), newDesc);
		
		try {
			// Convert date and time to millis
			SimpleDateFormat sdf = new SimpleDateFormat("d.M.yyy HH:mm");
			Date begin = sdf.parse(String.format("%s %s", dateBegin, timeBegin));
			Date end = sdf.parse(String.format("%s %s", dateEnd, timeEnd));
			
			entry.put(api.getEvent(CalendarApi.EVENT_DTSTART), begin.getTime());
			entry.put(api.getEvent(CalendarApi.EVENT_DTEND), end.getTime());
		} catch(ParseException e) {
			throw new IOException(e);
		}
		
		// Add shared
		entry.put(api.getEvent(CalendarApi.EVENT_CALENDAR_ID), mCalendarId);
		entry.put(api.getEvent(CalendarApi.EVENT_EVENT_TIMEZONE), "Europe/Helsinki");
		return entry;
	}
	
	private String readValue(String key, XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, key);
		String value = "";
		
		if(parser.next() == XmlPullParser.TEXT) {
			value = parser.getText();
			parser.nextTag();
		}
		
		return value;
	}
	
	private void skip(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		if(parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		
		int depth = 1;
		while(depth != 0) {
			switch(parser.next()) {
			case XmlPullParser.END_TAG:
				--depth;
				break;
				
			case XmlPullParser.START_TAG:
				++depth;
				break;
			}
		}
	}
}
