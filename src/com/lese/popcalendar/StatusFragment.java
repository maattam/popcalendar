package com.lese.popcalendar;

import com.lese.popcalendar.task.DeleteTask;
import com.lese.popcalendar.task.SynchronizeTask;
import com.lese.popcalendar.task.TaskEvent;
import com.lese.popcalendar.task.WorkTask;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class StatusFragment extends Fragment {
	public static final String NAME = "StatusFragment";
	
	private OnStatusChangedListener mCallback;
	public interface OnStatusChangedListener {
		public void onCompleted(String message);
		public void onCancelled();
		public void onError(String message);
		public void onLoginFailed();
	}
	
	public static final String ARG_DELETE = "delete";
	public static final String ARG_USER = "username";
	public static final String ARG_PASSWORD = "password";
	public static final String ARG_START = "start";
	public static final String ARG_END = "end";
	
	private WorkTask mTask = null;
	private TextView mStatusText;
	
	public static StatusFragment newDeleteInstance() {
		Bundle args = new Bundle();
		args.putBoolean(StatusFragment.ARG_DELETE, true);
		
		StatusFragment frag = new StatusFragment();
		frag.setArguments(args);
		return frag;
	}
	
	public static StatusFragment newInstance(String user, String password, long start, long end) {
		Bundle args = new Bundle();
		args.putString(StatusFragment.ARG_USER, user);
		args.putString(StatusFragment.ARG_PASSWORD, password);
		args.putLong(StatusFragment.ARG_START, start);
		args.putLong(StatusFragment.ARG_END, end);
		
		StatusFragment frag = new StatusFragment();
		frag.setArguments(args);
		return frag;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.layout_status, container, false);
		mStatusText = (TextView) view.findViewById(R.id.status_text);
		
		if(mTask == null) {
			// Set parameters
			Bundle args = getArguments();
			if(args != null) {
				boolean isdel = args.getBoolean(ARG_DELETE, false);
				if(!isdel) {
					String user = args.getString(ARG_USER);
					String password = args.getString(ARG_PASSWORD);
					long start = args.getLong(ARG_START);
					long end = args.getLong(ARG_END);
			
					mTask = new SynchronizeTask(user, password);
					mTask.initialize(new OnSynchronizeEvent(),
							getActivity().getContentResolver());
					mTask.execute(start, end);
			
				} else {
					mTask = new DeleteTask();
					mTask.initialize(new OnSynchronizeEvent(),
							getActivity().getContentResolver());
					mTask.execute((Long)null);
				}
			}
		}
		
		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		setRetainInstance(true);
		super.onCreate(savedInstanceState);
	}
	
	public void cancel(boolean clean) {
		if(mTask != null) {
			mStatusText.setText(getString(R.string.status_cancelling));
			mTask.cancel(false);
			
			if(clean) {
				mTask.initialize(null, null);
			}
		}
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mCallback = (OnStatusChangedListener) activity;
			
		if(mTask != null) {
			// Update pointers
			mTask.initialize(new OnSynchronizeEvent(), activity.getContentResolver());
		}
	}
	
	private class OnSynchronizeEvent implements TaskEvent {
		@Override
		public void statusUpdated(Event result) {
			switch(result) {
			case Authentication:
				mStatusText.setText(getString(R.string.status_authenticating));
				break;
				
			case Downloading:
				mStatusText.setText(getString(R.string.status_downloading));
				break;
				
			case Synchronizing:
				mStatusText.setText(getString(R.string.status_synchronizing));
				break;
				
			case Deleting:
				mStatusText.setText(getString(R.string.status_deleting));
				break;
				
			case CREATE_CALENDAR:
				Toast toast = Toast.makeText(getActivity(),
						"Created TUT POP Calendar.",
						Toast.LENGTH_LONG);
				toast.show();
			}
		}

		@Override
		public void completed(Result result, Object... data) {
			switch(result) {
			case Success:
				mCallback.onCompleted(String.format("New: %d | Updated: %d | Deleted: %d",
                        data[0], data[1], data[2]));
				break;
				
			case Cancel:
				mCallback.onCancelled();
				break;
				
			case Error:
				Exception err = (Exception)data[0];
				if(err.getMessage() != null) {
					mCallback.onError(err.getMessage());
				} else {
					mCallback.onError("Kapew! :(");
				}
				break;
				
			case ErrorLogin:
				mCallback.onLoginFailed();
				break;
			}
			
			mTask = null;
		}
	}
}
