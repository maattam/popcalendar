package com.lese.popcalendar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class InfoFragment extends Fragment {
	public static final String NAME = "InfoFragment";
	private TextView mTitle;
	private TextView mDetail;
	
	public static final String ARG_TITLE = "title";
	public static final String ARG_DETAIL = "detail";
	
	public static InfoFragment newInstance(String title, String detail) {
		Bundle args = new Bundle();
		args.putString(InfoFragment.ARG_TITLE, title);
		args.putString(InfoFragment.ARG_DETAIL, detail);
		
		InfoFragment frag = new InfoFragment();
		frag.setArguments(args);
		return frag;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_info, container, false);
		mTitle = (TextView) view.findViewById(R.id.info_title);
		mDetail = (TextView) view.findViewById(R.id.info_detail);
		
		Bundle args = getArguments();
		if(args != null) {
			mTitle.setText(args.getString(ARG_TITLE));
			mDetail.setText(args.getString(ARG_DETAIL));
		}
		
		return view;
	}
}
