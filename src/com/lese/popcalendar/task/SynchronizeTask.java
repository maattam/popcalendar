package com.lese.popcalendar.task;

import java.io.InputStream;

import com.lese.popcalendar.calendar.CalendarEventParser;
import com.lese.popcalendar.calendar.CalendarProvider;
import com.lese.popcalendar.calendar.POPHandler;

public final class SynchronizeTask extends WorkTask {
	
	private final String mUsername;
	private final String mPassword;
	
	public SynchronizeTask(String username, String password) {
		mUsername = username;
		mPassword = password;
	}
	
	@Override
	protected void onPreExecute() {
		publishProgress(TaskEvent.Event.Authentication);
	}
	
	@Override
	protected TaskEvent.Result doInBackground(Long... params) {
		Long start = params[0];
		Long end = params[1];
		
		if(isCancelled()) {
			return TaskEvent.Result.Cancel;
		}
		
		POPHandler pop = POPHandler.getInstance();
		
		try {
			CalendarProvider calp = new CalendarProvider(mContent);
			
			if(!pop.access(mUsername, mPassword)) {
				return TaskEvent.Result.ErrorLogin;
			}

			if(isCancelled()) {
				return TaskEvent.Result.Cancel;
			}
			
			publishProgress(TaskEvent.Event.Downloading);
			InputStream xml = pop.downloadCalendar(start, end);
			
			if(isCancelled()) {
				return TaskEvent.Result.Cancel;
			}
				
			publishProgress(TaskEvent.Event.Synchronizing);
			long calendarId = calp.getCalendarId();
			
			if(calendarId == -1) {
				calendarId = calp.createCalendar();
				publishProgress(TaskEvent.Event.CREATE_CALENDAR);
			}
				
			if(isCancelled()) {
				return TaskEvent.Result.Cancel;
			}
			
			CalendarEventParser parser = new CalendarEventParser(calendarId);
			mResult = calp.synchronize(start, end, parser.parse(xml));
		} catch (Exception e) {
			mError = e;
			return TaskEvent.Result.Error;
			
		} finally {
			pop.closeConnection();
		}

		return TaskEvent.Result.Success;
	}
}
