package com.lese.popcalendar.task;

import com.lese.popcalendar.calendar.CalendarProvider;
import com.lese.popcalendar.task.TaskEvent.Result;

public final class DeleteTask extends WorkTask {

	@Override
	protected void onPreExecute() {
		publishProgress(TaskEvent.Event.Deleting);
	}
	
	@Override
	protected Result doInBackground(Long... params) {
		try {
			CalendarProvider calp = new CalendarProvider(mContent);
			long calendarId = calp.getCalendarId();
			
			if(calendarId != -1) {
				mResult[2] = calp.clear();
				if(!calp.deleteCalendar()) {
					throw new Exception("Failed to delete calendar");
				}
			}
		} catch(Exception e) {
			mError = e;
			return TaskEvent.Result.Error;
		}
		
		return TaskEvent.Result.Success;
	}
	
}
