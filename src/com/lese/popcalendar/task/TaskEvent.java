package com.lese.popcalendar.task;

public interface TaskEvent {
	public enum Event { Authentication, Downloading, Synchronizing,
		Deleting, CREATE_CALENDAR };
	public enum Result { Success, Error, ErrorLogin, Cancel };
	
	public void statusUpdated(Event result);
	public void completed(Result result, Object... data);
}
