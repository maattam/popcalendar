package com.lese.popcalendar.task;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.os.AsyncTask;

public class UpdateCheckTask extends AsyncTask<Object, Object, String> {

	private static String REQUEST_URL = "http://www.students.tut.fi/~maatta3/popcalendar/latest.txt";
	
	private final OnUpdateCheckResult mHandler;
	public interface OnUpdateCheckResult {
		public void onUpdateResult(int revision, String url);
	}
	
	public UpdateCheckTask(OnUpdateCheckResult handler) {
		mHandler = handler;
	}
	
	@Override
	protected String doInBackground(Object... params) {
		HttpParams httpParams = new BasicHttpParams();
		httpParams.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		HttpConnectionParams.setConnectionTimeout(httpParams, 5000);
		HttpConnectionParams.setSoTimeout(httpParams, 10000);
		
		DefaultHttpClient client = new DefaultHttpClient(httpParams);
		
		try {
			HttpResponse response = client.execute(new HttpGet(REQUEST_URL));
			
			if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				
				String line = reader.readLine();
				response.getEntity().consumeContent();
				return line;
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			client.getConnectionManager().shutdown();
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		if(result != null) {
			String[] parts = result.split(" ");
			if(parts.length > 0) {
				try {
					mHandler.onUpdateResult(Integer.parseInt(parts[0]), parts[1]);
				} catch(Exception e) {
					
				}
			}
		}
		
		mHandler.onUpdateResult(-1, null);
	}
}
