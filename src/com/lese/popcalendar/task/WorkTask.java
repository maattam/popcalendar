package com.lese.popcalendar.task;

import android.content.ContentResolver;
import android.os.AsyncTask;

public abstract class WorkTask
	extends AsyncTask<Long, TaskEvent.Event, TaskEvent.Result> {
	
	protected TaskEvent mHandler = null;
	protected volatile ContentResolver mContent = null;
	
	public void initialize(TaskEvent handler, ContentResolver resolver) {
		mHandler = handler;
		mContent = resolver;
	}
	
	protected Exception mError = null;
	protected int[] mResult = new int[] {0, 0, 0};

	@Override
	protected void onProgressUpdate(TaskEvent.Event... results) {
		if(mHandler == null) {
			return;
		}
		
		if(results != null) {
			mHandler.statusUpdated(results[0]);
		}
	}

	@Override
	protected void onPostExecute(TaskEvent.Result result) {
		if(mHandler == null) {
			return;
		}
		
		if(result == TaskEvent.Result.Success) {
			mHandler.completed(result, mResult[0], mResult[1], mResult[2]);
		}
	
		else if(result == TaskEvent.Result.Error) {
			mHandler.completed(result, mError);
		}
	
		else {
			mHandler.completed(result);
		}
	}

	@Override
	protected void onCancelled() {
		if(mHandler == null) {
			return;
		}
		
		mHandler.completed(TaskEvent.Result.Cancel);
	}
}