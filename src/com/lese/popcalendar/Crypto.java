package com.lese.popcalendar;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

public class Crypto {
	
	public static String generateKey() throws NoSuchAlgorithmException {
		final int keyLength = 128;
		
		SecureRandom random = new SecureRandom();
		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(keyLength, random);
		SecretKey key = keyGen.generateKey();
		
		return Base64.encodeToString(key.getEncoded(), Base64.DEFAULT);
	}
	
	public static String encrypt(String key, String cleartext) throws Exception {
		byte[] rawKey = Base64.decode(key, Base64.DEFAULT);
		
		SecretKeySpec keySpec = new SecretKeySpec(rawKey, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);
		byte[] encrypted = cipher.doFinal(cleartext.getBytes());
		
		return Base64.encodeToString(encrypted, Base64.DEFAULT);
	}
	
	public static String decrypt(String key, String text) throws Exception {
		byte[] rawKey = Base64.decode(key, Base64.DEFAULT);
		byte[] encrypted = Base64.decode(text, Base64.DEFAULT);
		
		SecretKeySpec keySpec = new SecretKeySpec(rawKey, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		
		cipher.init(Cipher.DECRYPT_MODE, keySpec);
		byte[] decrypted = cipher.doFinal(encrypted);
		
		return new String(decrypted);
	}
}
