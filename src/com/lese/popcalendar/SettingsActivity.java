package com.lese.popcalendar;

import com.lese.popcalendar.task.UpdateCheckTask;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity {
	
	private int mRevision = -1;
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.settings);
		
		String appname = getString(R.string.app_name);
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			mRevision = pInfo.versionCode;
			appname += String.format(" %s.%d", pInfo.versionName, mRevision);
		} catch (NameNotFoundException e) {
		}
		
		findPreference("pref_appName").setTitle(appname);
		
		// Check if new update is already checked
		final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		if(settings.getInt("updateVersion", -1) > mRevision && mRevision != -1) {
			setUpdateText(settings.getString("updateUrl", null));
			
		} else {
			findPreference("pref_appUpdate").setOnPreferenceClickListener(new OnPreferenceClickListener() {

				@Override
				public boolean onPreferenceClick(Preference preference) {
					checkUpdate();
					return false;
				}
			});
		}
		
		findPreference("pref_saveLogin").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				if((Boolean)newValue == false) {
					SharedPreferences.Editor editor = settings.edit();
					editor.putString(MainActivity.PREF_USERNAME, "");
					editor.putString(MainActivity.PREF_PASSWORD, "");
					editor.commit();
				}
				
				return true;
			}
			
		});
	}
	
	@SuppressWarnings("deprecation")
	private void setUpdateText(final String url) {
		Preference pred = findPreference("pref_appUpdate");
		
		if(url == null) {
			pred.setTitle(R.string.prefs_error_update);
			return;
		}
		
		pred.setTitle(R.string.prefs_update_available);
		pred.setSummary(R.string.prefs_update_text);
		
		pred.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(browser);
				
				return false;
			}
		});
	}
	
	private void checkUpdate() {
		final ProgressDialog dialog = new ProgressDialog(this);
		dialog.setMessage(getString(R.string.prefs_update_dialog));
		
		UpdateCheckTask task = new UpdateCheckTask(new UpdateCheckTask.OnUpdateCheckResult() {

			@Override
			public void onUpdateResult(int revision, String url) {
				if(mRevision != -1 && revision > mRevision) {
					setUpdateText(url);
				}
				
				dialog.dismiss();
			}
			
		});
		
		task.execute((Object)null);
		dialog.show();
	}
}
