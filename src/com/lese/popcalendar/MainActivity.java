package com.lese.popcalendar;

import java.text.DateFormat;
import java.util.Calendar;

import com.lese.popcalendar.R;
import com.lese.popcalendar.calendar.POPHandler;
import com.lese.popcalendar.task.UpdateCheckTask;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;

public class MainActivity extends FragmentActivity
		implements MainFragment.OnMainListener,
		StatusFragment.OnStatusChangedListener {
	
	private TextView mLastSync;
	private SharedPreferences mPrefs = null;
	
	public static final String PREF_USERNAME = "prefs_username";
	public static final String PREF_PASSWORD = "prefs_password";
	public static final String PREF_SECRET = "prefs_secret";
	
	public static MainFragmentBundle mMainBundle = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Preserve bundle on configuration change
		if(mMainBundle == null) {
			mMainBundle = new MainFragmentBundle();
		}
		
		// Restore last sync date
		mLastSync = (TextView) findViewById(R.id.text_lastSync);
		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		mLastSync.setText(getString(R.string.text_lastsync)
				+ " " + mPrefs.getString("lastSyncDate", "Never"));
		
		// If last update is more than 2 days old, run update
		long lastUpdate = mPrefs.getLong("lastUpdateCheck", 0);
		long diff = Calendar.getInstance().getTimeInMillis() - lastUpdate;
		long days = diff / (24*60*60*1000);
		if(days > 2) {
			runUpdateCheck();
		}
		
		// Shrink header in landscape mode
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			final float scale = getResources().getDisplayMetrics().density;
			
			ImageView logo = (ImageView) findViewById(R.id.image_logo);
			LayoutParams lp = logo.getLayoutParams();
			lp.height = (int)(64 * scale + 0.5f);
			lp.width = lp.height;
			logo.setLayoutParams(lp);
		}
		
		// Set fragment
		if(findViewById(R.id.fragment_container) != null) {
			// Check if we are being restored from a previous state
			if(savedInstanceState != null) {
				return;
			}
			
			Bundle args = new Bundle();
			if(mPrefs.getBoolean("pref_saveLogin", false)) {
				// Load password from store
				try {
					String key = getSecretKey();
					args.putString(MainFragment.ARG_USERNAME,
							Crypto.decrypt(key, mPrefs.getString(PREF_USERNAME, "")));
					
					args.putString(MainFragment.ARG_PASSWORD,
							Crypto.decrypt(key, mPrefs.getString(PREF_PASSWORD, "")));
					
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			// Create main view
			MainFragment main = new MainFragment();
			
			// Apply arguments
			main.setArguments(args);
			
			// Add the fragment to container
			getSupportFragmentManager().beginTransaction()
				.setCustomAnimations(R.anim.fade_in, R.anim.fade_out,
						R.anim.fade_in, R.anim.fade_out)
				.add(R.id.fragment_container, main, MainFragment.NAME)
				.commit();
		}
	}
	
	private void runUpdateCheck() {
		final Context context = this;
		final SharedPreferences.Editor editor = mPrefs.edit();
		editor.putLong("lastUpdateCheck", Calendar.getInstance().getTimeInMillis());
		
		UpdateCheckTask task = new UpdateCheckTask(new UpdateCheckTask.OnUpdateCheckResult() {
			
			@Override
			public void onUpdateResult(int revision, String url) {
				int rev = -1;
				
				try {
					rev = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
				} catch (NameNotFoundException e) {
				}
				
				// Update available, set preference
				if(rev != -1 && revision > rev) {
					editor.putString("updateUrl", url);
					editor.putInt("updateVersion", revision);
					
					// Show some toast
					Toast toast = Toast.makeText(context,
							getString(R.string.update_available), Toast.LENGTH_LONG);
					toast.show();
				}
				
				editor.commit();
			}
		});
		
		task.execute((Object)null);
	}
	
	private void updateLastSyncDate() {
		DateFormat df = DateFormat.getDateTimeInstance(
				DateFormat.LONG, DateFormat.SHORT);
		String now = df.format(Calendar.getInstance().getTime());
		mLastSync.setText(getString(R.string.text_lastsync) + " " + now);
		
		// Update preference
		SharedPreferences.Editor editor = mPrefs.edit();
		editor.putString("lastSyncDate", now);
		editor.commit();
	}
	
	private void showFragment(Fragment fragment, String name) {
		// Save main fragment bundle
		Fragment main = getFragment(MainFragment.NAME);
		if(main != null) {
			((MainFragment)main).requestSaveBundle(mMainBundle);
		}
		
		FragmentTransaction transaction
			= getSupportFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out,
				R.anim.fade_in, R.anim.fade_out);
		transaction.replace(R.id.fragment_container, fragment, name);
		transaction.commit();
	}
	
	private Fragment getFragment(String name) {
		return getSupportFragmentManager().findFragmentByTag(name);
	}
	
	@Override
	public void onBackPressed() {
		// Are we in status?
		StatusFragment status = (StatusFragment)
				getFragment(StatusFragment.NAME);
		
		if(status != null) {
			status.cancel(false);
		}
		
		// Are we in info?
		else if(getFragment(InfoFragment.NAME) != null) {
			showFragment(MainFragment.newInstance(mMainBundle.get()), MainFragment.NAME);
		}
		
		// Reset session and bail out
		else {
			mMainBundle = null;
			POPHandler.closeInstance();
			finish();
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		StatusFragment status = (StatusFragment)
				getFragment(StatusFragment.NAME);
		
		if(status != null) {
			status.cancel(true);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		StatusFragment status = (StatusFragment)
				getFragment(StatusFragment.NAME);
		
		if(status != null) {
			showFragment(MainFragment.newInstance(mMainBundle.get()), MainFragment.NAME);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// If we are not in status fragment, bail out
		if(getFragment(StatusFragment.NAME) != null) {
			return true;
		}
		
		if(item.getItemId() == R.id.menu_delete) {
			showFragment(StatusFragment.newDeleteInstance(), StatusFragment.NAME);
			
		} else if(item.getItemId() == R.id.menu_settings) {
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivityForResult(intent, 0);
		}
		
		return true;
	}

	@Override
	public void onCompleted(String message) {
		showFragment(InfoFragment.newInstance("Success!", message), InfoFragment.NAME);
		updateLastSyncDate();
	}

	@Override
	public void onCancelled() {
		// Return to main view
		showFragment(MainFragment.newInstance(mMainBundle.get()), MainFragment.NAME);
	}

	@Override
	public void onError(String message) {
		showFragment(InfoFragment.newInstance("Oops!", message), InfoFragment.NAME);
	}

	@Override
	public void onLoginFailed() {
		// Return to main view
		mMainBundle.setError(true);
		showFragment(MainFragment.newInstance(mMainBundle.get()), MainFragment.NAME);
		mMainBundle.setError(false);
	}
	
	public class MainFragmentBundle {
		private Bundle args = new Bundle();
		
		public void set(String user, String password, long start, long end) {
			args.putString(MainFragment.ARG_USERNAME, user);
			args.putString(MainFragment.ARG_PASSWORD, password);
			args.putLong(MainFragment.ARG_DTSTART, start);
			args.putLong(MainFragment.ARG_DTEND, end);
		}
		
		public void setError(boolean value) {
			args.putBoolean(MainFragment.ARG_ERROR, value);
		}
		
		public final Bundle get() {
			return new Bundle(args);
		}
	}

	@Override
	public void onSynchronize(String user, String password, long start, long end) {
		mMainBundle.set(user, password, start, end);
		
		// If save login is checked, preserve current settings
		if(mPrefs.getBoolean("pref_saveLogin", false)) {
			// Store login
			try {
				String key = getSecretKey();
				SharedPreferences.Editor editor = mPrefs.edit();
				editor.putString(PREF_USERNAME, Crypto.encrypt(key, user));
				editor.putString(PREF_PASSWORD, Crypto.encrypt(key, password));
				editor.commit();
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		StatusFragment frag = StatusFragment.newInstance(user, password, start, end);
		showFragment(frag, StatusFragment.NAME);
	}
	
	private String getSecretKey() throws Exception {
		String key = mPrefs.getString(PREF_SECRET, null);
		
		// Generate new key if none exists
		if(key == null) {
			key = Crypto.generateKey();
			
			// Store key
			SharedPreferences.Editor editor = mPrefs.edit();
			editor.putString(PREF_SECRET, key);
			editor.commit();
		}
		
		return key;
	}
}
