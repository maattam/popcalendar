package com.lese.popcalendar;

import java.text.DateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class MainFragment extends Fragment {
	public static final String NAME = "MainFragment";
	public static final String ARG_ERROR = "errorlogin";
	public static final String ARG_USERNAME = "username";
	public static final String ARG_PASSWORD = "password";
	public static final String ARG_DTSTART = "dtstart";
	public static final String ARG_DTEND = "dtend";
	
	private OnMainListener mCallback;
	public interface OnMainListener {
		public void onSynchronize(String user, String password,
				long start, long end);
	}
	
	private Calendar mDateFrom = null;
	private Calendar mDateTo = null;
	
	private EditText mEditFrom;
	private EditText mEditTo;
	private EditText mEditPassword;
	private EditText mEditUsername;
	
	public static MainFragment newInstance(Bundle args) {
		MainFragment frag = new MainFragment();
		if(args != null) {
			frag.setArguments(args);
		}
		
		return frag;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.layout_main, container, false);
		
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			// Apply hackfix to relativeview
			LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			view.setLayoutParams(lp);
		}
		
		mEditFrom = (EditText) view.findViewById(R.id.edit_from);
		mEditTo = (EditText) view.findViewById(R.id.edit_to);
		mEditPassword = (EditText) view.findViewById(R.id.edit_password);
		mEditUsername = (EditText) view.findViewById(R.id.edit_username);
		
		mDateFrom = Calendar.getInstance();
		mDateTo = (Calendar) mDateFrom.clone();
		mDateTo.add(Calendar.MONTH, 3);
		
		// Fill forms with old data
		Bundle args = getArguments();
		if(args != null) {
			if(args.getBoolean(ARG_ERROR, false)) {
				setErrorLogin();
			}
			
			if(args.containsKey(ARG_USERNAME)) {
				mEditUsername.setText(args.getString(ARG_USERNAME));
			}
			
			if(args.containsKey(ARG_PASSWORD)) {
				mEditPassword.setText(args.getString(ARG_PASSWORD));
				
			}
			
			if(args.containsKey(ARG_DTSTART)) {
				mDateFrom.setTimeInMillis(args.getLong(ARG_DTSTART));
				
			}
			
			if(args.containsKey(ARG_DTEND)) {
				mDateTo.setTimeInMillis(args.getLong(ARG_DTEND));
			}
		}
		
		updateDate(mEditFrom, mDateFrom);
		updateDate(mEditTo, mDateTo);
		
		Button syncButton = (Button)view.findViewById(R.id.button_sync);
		syncButton.setOnClickListener(
				new View.OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						attemptSync();
					}
				});
		
		mEditFrom.setOnClickListener(
				new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						showDatePicker(mEditFrom, mDateFrom);
					}
				});
		
		mEditTo.setOnClickListener(
				new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						showDatePicker(mEditTo, mDateTo);
					}
				});
		
		return view;
	}
	
	public void setErrorLogin() {
		mEditPassword.setError(getString(R.string.error_incorrect_password));
		mEditPassword.requestFocus();
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mCallback = (OnMainListener) activity;
	}
	
	private static void updateDate(EditText view, Calendar date) {
		DateFormat df = DateFormat.getDateInstance();
		view.setText(df.format(date.getTime()));
	}
	
	private void attemptSync() {
		// Set times
		mDateFrom.set(Calendar.HOUR_OF_DAY, 0);
		mDateFrom.set(Calendar.MINUTE, 0);
		mDateTo.set(Calendar.HOUR_OF_DAY, 23);
		mDateTo.set(Calendar.MINUTE, 59);
		
		if(mDateTo.compareTo(mDateFrom) < 0) {
			// Display nag
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(getString(R.string.alert_date));
			builder.setPositiveButton("OK", null);
			builder.show();
		}
		
		else if(mEditUsername.getText().length() == 0 ||
				mEditPassword.getText().length() == 0) {
			setErrorLogin();
		}
		
		else {
			String username = mEditUsername.getText().toString();
			String password = mEditPassword.getText().toString();
			
			mCallback.onSynchronize(username, password,
					mDateFrom.getTimeInMillis(),
					mDateTo.getTimeInMillis());
		}
	}
	
	private void showDatePicker(EditText view, Calendar date) {
		DatePickerDialog dlg = new DatePickerDialog(getActivity(),
				new DatePickerListener(view, date),
				date.get(Calendar.YEAR),
				date.get(Calendar.MONTH),
				date.get(Calendar.DAY_OF_MONTH));
		dlg.show();
	}
	
	private class DatePickerListener implements DatePickerDialog.OnDateSetListener {
		
		private EditText mView;
		private Calendar mDate;
		
		public DatePickerListener(EditText view, Calendar date) {
			mView = view;
			mDate = date;
		}
		
		public void onDateSet(DatePicker view, int year, int month, int day) {
			mDate.set(Calendar.YEAR, year);
			mDate.set(Calendar.MONTH, month);
			mDate.set(Calendar.DAY_OF_MONTH, day);
			
			updateDate(mView, mDate);
		}
	}
	
	public void requestSaveBundle(MainActivity.MainFragmentBundle bundle) {
		String username = mEditUsername.getText().toString();
		String password = mEditPassword.getText().toString();
		
		bundle.set(username, password, mDateFrom.getTimeInMillis(), mDateTo.getTimeInMillis());
	}
}
